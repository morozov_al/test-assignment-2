from .models import Product
from .serializers import (
    ProductSerializer,
    ProductCardSerializer,
)
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from django_filters.rest_framework import DjangoFilterBackend
from .filters import ProductFilter


class ProductPagination(PageNumberPagination):
    page_size = 2


class ProductByCategoryView(ListAPIView):
    serializer_class = ProductSerializer
    pagination_class = ProductPagination
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = ProductFilter

    def get_queryset(self):
        category_id = self.kwargs['category_id']
        products = Product.objects.filter(categories=category_id)
        return products


class ProductCardView(ListAPIView):
    serializer_class = ProductCardSerializer

    def get_queryset(self):
        product_id = self.kwargs['product_id']
        products = Product.objects.filter(id=product_id)
        return products



