from django.urls import path, include
from .views import ProductByCategoryView, ProductCardView

app_name = "myapp"

urlpatterns = [
    path('category/<int:category_id>/', ProductByCategoryView.as_view(), name='products-by-category'),
    path('product/<int:product_id>/', ProductCardView.as_view(), name='products-info'),
]
