from django.db import models


class Manufacturer(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Characteristic(models.Model):
    name = models.CharField(max_length=255)
    is_primary = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class CharacteristicValue(models.Model):
    characteristic = models.ForeignKey(Characteristic, on_delete=models.CASCADE)
    value = models.CharField(max_length=255)

    def __str__(self):
        return self.value


class Category(models.Model):
    name = models.CharField(max_length=255)
    parent_category = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)
    category_characteristic = models.ManyToManyField(Characteristic)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    categories = models.ManyToManyField(Category)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    characteristics = models.ManyToManyField(CharacteristicValue)

    def __str__(self):
        return self.name
