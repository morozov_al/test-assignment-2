# filters.py

import django_filters
from .models import Product, Characteristic


class CharacteristicValueFilter(django_filters.BaseInFilter, django_filters.CharFilter):
    pass


class ProductFilter(django_filters.FilterSet):
    class Meta:
        model = Product
        fields = ['categories']

    # Добавьте поля фильтрации для характеристик
    characteristic_name = django_filters.CharFilter(field_name='characteristics__characteristic__name', lookup_expr='icontains', label='Characteristic Name')
    characteristic_value = django_filters.CharFilter(field_name='characteristics__value', lookup_expr='icontains', label='Characteristic Value')