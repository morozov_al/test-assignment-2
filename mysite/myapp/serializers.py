from rest_framework import serializers
from .models import Product, CharacteristicValue


class CharacteristicSerializer(serializers.ModelSerializer):
    characteristic = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = CharacteristicValue
        fields = ['characteristic', 'value']


class ProductSerializer(serializers.ModelSerializer):
    characteristics = serializers.SerializerMethodField()
    categories = serializers.StringRelatedField(many=True, read_only=True)
    manufacturer = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Product
        fields = "__all__"

    def get_characteristics(self, product):
        filtered_characteristics = product.characteristics.exclude(characteristic__is_primary=False)
        characteristic_serializer = CharacteristicSerializer(filtered_characteristics, many=True)
        return characteristic_serializer.data


class ProductCardSerializer(serializers.ModelSerializer):
    characteristics = CharacteristicSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = (
            'name',
            'characteristics'
        )