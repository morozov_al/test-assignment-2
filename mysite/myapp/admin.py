from django.contrib import admin
from .models import Manufacturer, Category, Characteristic, CharacteristicValue, Product


class CharacteristicValueInline(admin.TabularInline):
    model = CharacteristicValue
    extra = 1


class ProductAdmin(admin.ModelAdmin):
    filter_horizontal = ('categories', 'characteristics')


class CategoryAdmin(admin.ModelAdmin):
    filter_horizontal = ('category_characteristic',)


admin.site.register(Manufacturer)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Characteristic)
admin.site.register(CharacteristicValue)
admin.site.register(Product, ProductAdmin)
